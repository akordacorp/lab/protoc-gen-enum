# protoc-gen-enum

This package implements an opinionated protoc plugin that generates enum methods
to print custom string representations of enum values.

In particular, this plugin is used to generate reverse DNS style strings to be used
as "event types" in [CloudEvents](https://github.com/cloudevents).

The code is available through Buf's Schema Registry at https://buf.build/akorda/protoc-gen-enum.

## Usage

### Buf

We recommend the use of [buf](https://buf.build/) to generate the code. The rest
of this section assumes you're using it to build your project.

First, get the plugin:
```shell
go get -u gitlab.com/akordacorp/lab/protoc-gen-enum/cmd/protoc-gen-go-enum
```

Then, annotate your enum with the `event_name` option as follows:

```protobuf

import "enum/enum.proto";

// ExampleEvent
enum ExampleEvent{
  option (enum.event_name) = true;

  EXAMPLE_EVENT_UNSPECIFIED = 0;
  EXAMPLE_EVENT_CREATED = 1;
}
```

Then add `buf.build/akorda/protoc-gen-enum` as a dependency in your `buf` project
then run `buf mod update` and finally generate your project.

Note that the repository is private so you'll need a valid API token stored in
the `BUF_TOKEN` env variable.


### Protoc

```shell
protoc --go_out=. --go_opt=paths=source_relative \
    --go-enum_out=. --go-enum_opt=paths=source_relative,prefix=com.example \
    myproto.proto
```

For a complete example see the `examples/` directory.

## Events Format

This plugin assumes the enum names end with `Event` and that the enum values are
prefixed with the enum name. It will error out otherwise.

We recommend you use a `proto` linter like `buf` with the `ENUM_VALUE_PREFIX`
option turned on.
