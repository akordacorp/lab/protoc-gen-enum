PKG_LIST := $(shell go list ./... | grep -v 'cache\|docs\|mocks\|examples\|cmd')
COLOR := "\e[1;36m%s\e[0m\n"

## Get the test deps
test-deps:
	@echo "installing test dependencies..."
	@go install github.com/axw/gocov/gocov@v1.0.0
	@go install github.com/AlekSi/gocov-xml@v1.0.0
	@go install github.com/matm/gocov-html@latest
	@mkdir -p test-artifacts/coverage

## Run coverage for the project
coverage:
	@cat test-artifacts/gocov.json | gocov report
	@cat test-artifacts/gocov.json | gocov-xml > test-artifacts/coverage/coverage.xml
	@cat test-artifacts/gocov.json | gocov-html > test-artifacts/coverage/coverage.html

## We use the -short flag to avoid running Integration tests
test-short: test-deps
	@gocov test -short ${PKG_LIST} -covermode=atomic -coverpkg=./... > test-artifacts/gocov.json

## Run all the available tests
test-long: test-deps
	@gocov test ${PKG_LIST} -covermode=atomic -coverpkg=./... > test-artifacts/gocov.json
	@cat test-artifacts/gocov.json | gocov report
	@cat test-artifacts/gocov.json | gocov-xml > test-artifacts/coverage/coverage-integration.xml
	@cat test-artifacts/gocov.json | gocov-html > test-artifacts/coverage/coverage-integration.html

## Run only integration tests
test-integration: test-deps
	@gocov test -run Integration ${PKG_LIST} -covermode=atomic -coverpkg=./... > test-artifacts/gocov.json
	@cat test-artifacts/gocov.json | gocov report
	@cat test-artifacts/gocov.json | gocov-xml > test-artifacts/coverage/coverage-integration.xml
	@cat test-artifacts/gocov.json | gocov-html > test-artifacts/coverage/coverage-integration.html

## Run all tests and generate a coverage report
test: test-short coverage

# ref: https://docs.git	lab.com/ee/development/go_guide/
lint:
	@echo "installing lint deps.."
	@wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s latest
	@golangci-lint run --skip-dirs-use-default --out-format code-climate | tee gl-lint-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'

lint-local:
	@golangci-lint run --fix --skip-dirs-use-default --out-format code-climate | tee gl-lint-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'

proto:
	@protoc --go_out=. --go_opt=paths=source_relative enum/enum.proto

proto-example:
	@echo "building example.."
	go get -u gitlab.com/akordacorp/lab/protoc-gen-enum/cmd/protoc-gen-go-enum
	protoc --go_out=. --go_opt=paths=source_relative --go-enum_out=. --go-enum_opt=paths=source_relative,prefix=com.akorda example/example.proto example/example2.proto

buf-install:
	@printf $(COLOR) "Install/update buf..."
	@go install github.com/bufbuild/buf/cmd/buf@v1.0.0-rc11

push: buf-install
	buf push

##### CLEANUP
clean:
	rm -rf example/*pb.go
	rm -rf ./test-artifacts