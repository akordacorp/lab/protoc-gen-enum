module gitlab.com/akordacorp/lab/protoc-gen-enum

go 1.17

require google.golang.org/protobuf v1.27.1

require github.com/golang/protobuf v1.5.2 // indirect
