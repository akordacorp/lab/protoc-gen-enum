package example

import (
	"testing"
)

func TestExample(t *testing.T) {

	sEvent := SavedSearchEvent_SAVED_SEARCH_EVENT_CREATED.EventName()
	if sEvent != "com.akorda.saved_search.created" {
		t.Error("invalid enum value")
	}

	dEvent := DocumentEvent_DOCUMENT_EVENT_CREATED.EventName()
	if dEvent != "com.akorda.document.created" {
		t.Errorf("invalid enum value %s", dEvent)
	}

	eEvent := ExampleEvent_EXAMPLE_EVENT_RESOURCE_UPDATED.EventName()
	if eEvent != "com.akorda.example.resource_updated" {
		t.Errorf("invalid enum value %s", eEvent)
	}
}